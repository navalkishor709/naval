package com.example.hp.naval;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;

public class MainActivity extends AppCompatActivity {
BottomNavigationBar navigationBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationBar=findViewById(R.id.bottombar);
        BottomBarItem one = new BottomBarItem(R.drawable.home);
        BottomBarItem two = new BottomBarItem(R.drawable.user);
        BottomBarItem three = new BottomBarItem(R.drawable.veg);


        navigationBar.addTab(one);
        navigationBar.addTab(two);
        navigationBar.addTab(three);

        addFragment(new FragmentHome());
        navigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                switch (position)
                {
                    case 0:
                    {
                        replaceFragment(new FragmentHome());
                        break;
                    }
                    case 1:
                    {
                        replaceFragment(new FragmentVeg());
                        break;
                    }
                    case 2:
                    {
                        replaceFragment(new FragmentUser());
                        break;
                    }
                }
            }
        });

    }
    public void  addFragment(Fragment fragment)
    {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container,fragment);
        transaction.commit();
    }
    public void  replaceFragment(Fragment fragment)
    {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container,fragment);
        transaction.commit();
    }


}


