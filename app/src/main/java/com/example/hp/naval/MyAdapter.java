package com.example.hp.naval;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.HOLDER> {

    @NonNull
    @Override
    public HOLDER onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view,viewGroup,false);
        return new HOLDER(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HOLDER holder, int i) {

    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public class HOLDER extends RecyclerView.ViewHolder {
        public HOLDER(@NonNull View itemView) {
            super(itemView);
        }
    }
}
